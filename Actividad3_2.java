/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad3_2;

//Hola buenas tardes soy julio

import java.sql.*;

/**
 * Comentario
 * @author loren
 */
public class Actividad3_2 {
    //prueba
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        Connection conexion = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conexion = DriverManager.getConnection("jdbc:sqlite:concursomusica.db");
            System.out.println("Conexion OK con concursomusica.db");
            insertarGrupo(conexion);
            consultarGrupos(conexion);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (conexion != null) {
            conexion.close();
        }

    }
        //Crear ConsultarGrupos
    private static void consultarGrupos(Connection conexion) {
        try {
            //mediante conexion, crear un objeto Statement para enviar
            // instrucciones SQL sin parámetros
            Statement st = conexion.createStatement();
            // crear texto de instruccion SQL y enviar
            //Devuelve un conjunto de resultados
            String txtSQL = "SELECT nombregrupo, localidad, fechaPrimerConcierto FROM grupos";
            ResultSet result = st.executeQuery(txtSQL);
            //Recorrer todas las filas de result, desde la primera
// avanzando de una en una hacia la siguiente
            while (result.next()) {
                // Saca el valor de la columna nombregrupo
                String nom = result.getString("nombregrupo");
// Saca el valor de la columna segunda de la consulta (localidad)
                String loc = result.getString(2);
                String fec = result.getString("fechaPrimerConcierto");
                System.out.println("NOMBRE: " + nom + ",  LOCALIDAD: " + loc + " FECHA: " + fec);
            }
            st.close();
            result.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void insertarGrupo(Connection conexion) {
//. . . . Recoger datos en nom,loc, esgrupo, fechaP, annoD
        String nom = "Amaral";
        String loc = "Zaragoza";
        int esgrupo = 1;
        String fechaP = "1999-01-08";
        int annoD = 1999;

//Crear texto de consulta con parámetros sustituibles
        String textInsert = "INSERT INTO grupos (nombregrupo,localidad,"
                + "esgrupo,fechaPrimerConcierto,annoPrimerDisco) "
                + "VALUES (?,?,?,?,?);";
        try {
            //Construir un PreparedStatement para sustituir valores 
            // en consulta parametrizada
            PreparedStatement prepSt = conexion.prepareStatement(textInsert);

            // . . . . Aquí se sustituyen valores por las ? y se envía para 
            // su ejecución la instrucción con los parámetros ya sustituidos
            // Sustituye la ? primera por el contenido de nom
            prepSt.setString(1, nom);
// Sustituye la ? segunda por el contenido de loc
            prepSt.setString(2, loc);
            prepSt.setInt(3, esgrupo);
            prepSt.setString(4, fechaP);
            prepSt.setInt(5, annoD);

//Ejecutar la instrucción y obtener cuantas filas han sido afectadas;
            int n = prepSt.executeUpdate();
            System.out.println("Se han insertado " + n + " grupos");
            prepSt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
